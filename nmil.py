import math
import json
import random
import numpy as np
import pandas as pd
from tqdm import tqdm

from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score, roc_auc_score

def sigmoid(x):
    return 1. / (1. + math.exp(-x))

def data_to_numpy(path_to_csv):
    
    dataset = pd.read_csv(path_to_csv)
    bag_id = dataset.groupby('bagID', sort=False)
    bag_keys = list(bag_id.groups.keys())
    
    x_data = []
    y_data = []
    id_data = []

    for bag_key in tqdm(bag_keys):
        x_bag = []
        id_bag = []
        bag = bag_id.get_group(bag_key)
        day_id = bag.groupby('day', sort=False)
        day_keys = list(day_id.groups.keys())
        for day_key in day_keys:
            x_day = []
            id_day = []
            y = []
            days = day_id.get_group(day_key)
            for day in days.iterrows():
                id_day.append(day[1]['instanceID'])
                x_day.append(np.asarray(json.loads(day[1]['feature_2'])))
                y.append(day[1]['bagLabel'])
            x_bag.append(np.asarray(x_day))
            id_bag.append(np.asarray(id_day))
        x_data.append(x_bag)
        id_data.append(id_bag)
        y_label = list(set(y))
        assert len(y_label) == 1
        y_data.append(y_label[0])
        
    return np.asarray(x_data), np.asarray(y_data), np.asarray(id_data)
    
    
def gradient_function(X, Y, w, beta = 3.0, m0 =  0.5, p0 = 0.5, gamma = 0.5):
    
    '''
    For the mathematical comprehension of this gradient descent implementation geared for multi-instance learning,
    please refer to:
    Ning, Y., Muthiah, S., Rangwala, H., & Ramakrishnan, N. (2016, August). 
    Modeling precursors for event forecasting via nested multi-instance learning. 
    In Proceedings of the 22nd ACM SIGKDD international conference on knowledge discovery and data mining (pp. 1095-1104).
    
    The first, second, and third matrices, respectively, represent the loss function, the cross-bag cost, and the instancecost.
    Please follow the comments below to trace the equations mentioned in the paper.
    '''

    #For the loss function
    first_matrix = []
    #For the cross-bag cost
    second_matrix = []
    #For the instance cost
    third_matrix = []

    for i, superbag in enumerate(X):
        jk_plus = []
        P_i_list = []
        m_i = len(X[i])
        P_ijk_list = []
        n_ij_list = []

        for j, bag in enumerate(superbag):
            m_i = len(bag)
            #k_plus -> h(x_ij, w)
            k_plus = [k for k, doc in enumerate(bag) 
                    if np.sign(sigmoid(np.dot(w, doc[:, np.newaxis])) - p0) * np.dot(w, doc[:, np.newaxis]) < m0]
            #p_ij_list -> calculate p_ijs eqn (1)
            p_ij_list = [sigmoid(np.dot(w, doc[:, np.newaxis])) for k, doc in enumerate(bag)]
            jk_plus.append(k_plus)
            n_ij_list.append(m_i)
            P_ijk_list.append(p_ij_list)
            #p_i_list -> Bag probability eqn (2)
            p_ij = np.mean(p_ij_list)
            P_i_list.append(p_ij)

        #Superbag probability eqn (3)
        P_i = np.mean(P_i_list)

        #X[i] is 1 superbag 
        for j, x in enumerate(X[i]):
            if len(x) > 0:
                # k is every news article
                row_list = [X[i][j][k] * P_ijk_list[j][k]\
                            * (1. - P_ijk_list[j][k])\
                            * ((Y[i] - P_i) / (P_i * (1.- P_i)))\
                            * (1. / n_ij_list[j])\
                            for k in range(len(X[i][j]))]

                #row_list = doc_vec * pij * (1 - pij) * ((Y - Pi) / (Pi * (1 - Pi))) * (1 / length_of_bag)
                #For each document in bag
                #This is the main loss function

                # np.sum(np.array(row_list), axis=0) -> Gives a 384 dim representation of news for day t
                first_matrix.append(np.sum(np.array(row_list), axis=0))

        for j, x in enumerate(X[i]):
            if len(x) > 0:
                row_list = [(X[i][j][k]\
                            * P_ijk_list[j][k]\
                            * (1. - P_ijk_list[j][k])\
                            * (1. / n_ij_list[j]))\
                            for k in range(len(x))]
                if j > 0 and len(X[i][j-1]) > 0:
                    row_list2 = [(X[i][j-1][k]\
                                * P_ijk_list[j-1][k]\
                                * (1. - P_ijk_list[j-1][k])\
                                * (1. / n_ij_list[j-1]))\
                                for k in range(len(X[i][j-1]))]
                    current_sum = np.sum(np.array(row_list), axis=0)
                    last_sum = np.sum(np.array(row_list2), axis=0)

                    derivative = 2. * (1./ len(X[i]))\
                                    * (P_i_list[j] -  P_i_list[j-1])\
                                    * (current_sum - last_sum)\
                                    /(len(X[i][j]) * len(X[i][j-1]))
                    second_matrix.append(derivative)

        for idj, kplus in enumerate(jk_plus):
            if len(kplus) > 0:
                row_list = [X[i][idj][idk]\
                            * np.sign(P_ijk_list[idj][idk] - p0)\
                            * (1. / n_ij_list[idj])\
                            for idk in kplus]

                sum_row = np.sum(np.array(row_list), axis=0)
                third_matrix.append(sum_row * (1./ n_ij_list[idj]))


    first_sum = np.sum(np.array(first_matrix), axis=0) * beta
    second_sum = np.sum(np.array(second_matrix), axis=0) * gamma
    third_sum = np.sum(np.array(third_matrix), axis=0) * gamma


    if len(second_matrix) > 0 and len(third_matrix) > 0:
        return -first_sum + second_sum - third_sum 
    else:
        return -first_sum

def nmil_case_basis(train_x, test_x, train_y, test_y, id_train, id_test, lambd = 0.05, iteration = 2000, \
         x_dimension = 300, sig_threshold = 0.98, n_sgd = 1):
    
    metric_dict = {}
    test_X_pos = test_x[(test_y == 1).nonzero()[0]]
    test_y_pos = test_y[(test_y == 1).nonzero()[0]]
    test_X_neg = test_x[(test_y == 0).nonzero()[0]]
    test_y_neg = test_y[(test_y == 0).nonzero()[0]]
    
    for expr in range(n_sgd):

        w_p = np.random.rand(x_dimension)

        for t in tqdm(range(iteration)):

            eta = 1./((t + 1) * lambd)
            kset = random.sample(range(0, len(train_x) - 1), 10)

            X_sam = np.array([train_x[z] for z in kset])
            Y_sam = np.array([train_y[z] for z in kset])

            delta_w = gradient_function(X_sam, Y_sam, w_p)

            new_w = np.dot((1 - eta * lambd), w_p) - eta * delta_w / len(X_sam)

            rate = (1./np.sqrt(lambd)) * (1./np.linalg.norm(new_w))

            if  rate < 1.:
                w_p = np.dot(rate, new_w)
            else:
                w_p = new_w
                
        pred_y = []

        for idx, testx in enumerate(test_X_pos):
            p_ij_list = []
            for j, day in enumerate(testx):
                p_ijk_list = [sigmoid(np.dot(w_p, doc[:, np.newaxis])) for k, doc in enumerate(day)]
                p_ij_list.append(p_ijk_list)


            days_list = [np.mean(day_prob) for day_prob in p_ij_list]
            P_i =  np.mean(days_list)    
            if P_i > 0.5:
                pred_y.append(1)
            else:
                pred_y.append(0)

        #metric_dict["f1_pos_{}".format(expr)] = f1_score(test_y_pos, pred_y)
        metric_dict["accuracy_pos_{}".format(expr)] = accuracy_score(test_y_pos, pred_y)
        #metric_dict["precision_pos_{}".format(expr)] = precision_score(test_y_pos, pred_y)
        #metric_dict["recall_pos_{}".format(expr)] = recall_score(test_y_pos, pred_y)

        pred_y = []

        for idx, testx in enumerate(test_X_neg):
            p_ij_list = []
            for j, day in enumerate(testx):
                p_ijk_list = [sigmoid(np.dot(w_p, doc[:, np.newaxis])) for k, doc in enumerate(day)]
                p_ij_list.append(p_ijk_list)


            days_list = [np.mean(day_prob) for day_prob in p_ij_list]
            P_i =  np.mean(days_list)    
            if P_i > 0.5:
                pred_y.append(1)
            else:
                pred_y.append(0)

        #metric_dict["f1_neg_{}".format(expr)] = f1_score(test_y_neg, pred_y)
        metric_dict["accuracy_neg_{}".format(expr)] = accuracy_score(test_y_neg, pred_y)
        #metric_dict["precision_neg_{}".format(expr)] = precision_score(test_y_neg, pred_y)
        #metric_dict["recall_neg_{}".format(expr)] = recall_score(test_y_neg, pred_y)

    return metric_dict

def nmil(train_x, test_x, train_y, test_y, id_train, id_test, lambd = 0.05, iteration = 2000, \
                  x_dimension = 300, sig_threshold = 0.98, n_sgd = 1):
    
    metric_dict = {}
    precursors_dict = {}
    
    for expr in range(n_sgd):

        w_p = np.random.rand(x_dimension)

        for t in tqdm(range(iteration)):

            eta = 1./((t + 1) * lambd)
            kset = random.sample(range(0, len(train_x) - 1), 10)

            X_sam = np.array([train_x[z] for z in kset])
            Y_sam = np.array([train_y[z] for z in kset])

            delta_w = gradient_function(X_sam, Y_sam, w_p)

            new_w = np.dot((1 - eta * lambd), w_p) - eta * delta_w / len(X_sam)

            rate = (1./np.sqrt(lambd)) * (1./np.linalg.norm(new_w))

            if  rate < 1.:
                w_p = np.dot(rate, new_w)
            else:
                w_p = new_w

        pred_y = []
        sig_precursors = []
        sig_precursors_prob = []

        for idx, testx in enumerate(test_x):
            p_ij_list = []
            for j, day in enumerate(testx):
                p_ijk_list = [sigmoid(np.dot(w_p, doc[:, np.newaxis])) for k, doc in enumerate(day)]
                p_ij_list.append(p_ijk_list)

            precursors = []
            precursors_prob = []
            for ind_1, val in enumerate(p_ij_list):
                for ind_2, el in enumerate(val):
                    if el >= sig_threshold:
                        precursors.append(id_test[idx][ind_1][ind_2])
                        precursors_prob.append(el)
            sig_precursors.append(precursors)
            sig_precursors_prob.append(precursors_prob)


            days_list = [np.mean(day_prob) for day_prob in p_ij_list]
            P_i =  np.mean(days_list)    
            if P_i > 0.5:
                pred_y.append(1)
            else:
                pred_y.append(0)
        
        
        metric_dict["f1_run_{}".format(expr)] = f1_score(test_y, pred_y)
        metric_dict["accuracy_run_{}".format(expr)] = accuracy_score(test_y, pred_y)
        metric_dict["precision_run_{}".format(expr)] = precision_score(test_y, pred_y)
        metric_dict["recall_run_{}".format(expr)] = recall_score(test_y, pred_y)
        metric_dict["rocauc_run_{}".format(expr)] = roc_auc_score(test_y, pred_y)
        
        precursors_dict["precursors_run_{}".format(expr)] = sig_precursors
        precursors_dict["precursors_prob_{}".format(expr)] = sig_precursors_prob
        
    return metric_dict, precursors_dict