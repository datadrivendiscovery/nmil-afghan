# nmil-afghan



## Getting started

Welcome to Virginia Tech's nMIL Framework! Forecasting large-scale societal events like civil unrest movements, disease outbreaks, and elections is an important and challenging problem. From the perspective of human analysts and policy makers, forecasting algorithms must not only make accurate predictions but must also provide supporting evidence, e.g., the causal factors related to the event of interest. At Virginia Tech, we have developed a novel multiple instance learning based approach for D3M that jointly tackles the problem of identifying evidence-based precursors and forecasts events into the future.

> **Note:** As the Numpy-based embeddings of document vectors are larger in size, [Git LFS](https://git-lfs.github.com/) was used to push these models to Git. Please make sure you have it installed in your system.

Before using this repository, please make sure you have the required packages installed in your environment:
```
pip install -r requirements.txt
```

**This application adapts the nMIL framework in predicting the occurance of casualties in the ACLED dataset for events observed in Afghanistan and its associated territories.**

## Building Vectorized Datasets (Skippable)

The directory *Afghan* consists of pre-processed datasets for the 3 largest geo-clusters obtaiend from the ACLED dataset *Afghan.csv*. However, if you wish to build datasets for other geo-clusters or rebuild the dataset for the top 3 clusters yourself, you must first have the [Gensim](https://radimrehurek.com/gensim/) based Doc2Vec model placed inside the directory *Doc2Vec*. As the upload limit for this Gitlab is capped at 8GB, please download the models (approx ~10GB) [here](https://drive.google.com/drive/folders/1kDeS0j1DqIzz5Kj5KnjmQqJVslRzeVDm?usp=sharing). Please copy the contents (the .py, .npy, and .model files) from the Google drive inside the directory *Doc2Vec*.


## Running nMIL

The notebook *Afghan Casualties.ipynb* is self-contained and contains step-by-step explanations, from the construction of the dataset (skippable) to running the algorithm. If you have any questions, please reach out to mandarsharma@vt.edu.

Thanks!
